# Spectral

Hugo threme based on html5up’s [Spectral](https://html5up.net/spectral) theme.

An [original port](https://github.com/sbruder/spectral) of the theme to Hugo was done by [Simon Bruder](https://github.com/sbruder).

## Changes and fixes

The following fixes and changes were applied to the original theme version:
- Fix of displaying background image for the top page section
- Customizeble header is added to the top bunner
- Add of possibility to disable:
  - Menu;
  - Learn More button;
  - Background image for the top page section.
- favicon is added as png file in static directory


## Installation

If this is your first hugo page, please read the [Basic Usage of
Hugo](https://gohugo.io/getting-started/usage/) article.

Clone this repo or download the zip, place it inside of the `themes` directory
of your hugo site and use the `config.toml` file located in exampleSite as a
starting point.

You are able to quick run preview of the theme on your environment as well.

Just clone this repo or download the zip, go to the theme root directory and run command:

```
hugo server -s exampleSite/ --themesDir ../.. -d ../public
```

Site demo will be available on [http://localhost:1313](http://localhost:1313)

## Examples

### Changed theme

Its latest live build can be seen at <https://k-2-tech.gitlab.io/hugo/spectral>.

A real life installation <https://k-2.tech>

### Original theme

Its latest live build can be seen at <https://sbruder.github.io/spectral/>.

A real life installation: [kegelschiene.net](https://git.sbruder.de/kegelschiene/site). It also includes
the configuration for a multilingual site.


## License

This theme is licensed under the terms of the Creative Commons Attribution 3.0
Unported (like html5up’s original theme). See the LICENSE.md file for more
information on this.
